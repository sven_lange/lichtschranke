# Lichtschranke - Track & Field Light Barrier Time Measuring



## Overview

This repository contains source code for the Time Tracking solution, we build for our sprint training.
This solution uses two light barriers to track the time interval between the first and second light barrier.

Many software features are missing right now, but we are working on it :)

## Roadmap
- Uploading already exististing files.
- Feature: Multiple Times

## License
GNU GPLv3 

## Project status
Hardware - ready.
Software - In Development - Serial Output works.